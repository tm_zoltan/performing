var gulp         = require("gulp");
var browserSync  = require("browser-sync").create();
var sass         = require("gulp-sass");
var watch        = require("gulp-watch");
// var path         = require("gulp-path");
// var folders      = require('gulp-folders');
// var	pathToFolder = 'path/to/folder';
//
// gulp.task('task', folders(pathToFolder, function(folder){
// 	//This will loop over all folders inside pathToFolder main, secondary
// 	//Return stream so gulp-folders can concatenate all of them
// 	//so you still can use safely use gulp multitasking
//
// 	return gulp.src(path.join(pathToFolder, folder, '*.js'))
// 		.pipe(concat(folder + '.js'))
// 		.pipe(gulp.dest('dist'));
// }));

gulp.task("sass", function () {
  return gulp.src("./app/scss/**/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("./dist/css"))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch', ['browserSync', 'sass'], function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/**/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
})

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    },
  })
})

gulp.task('build', ['sass'], function (){
    console.log('Building files');
})

gulp.task("default", ["sass", "serve"]);

// gulp.task("serve", ["sass"], function() {
//     gulp.watch("app/scss/**/*.scss", ["sass"]);
//     gulp.watch("app/**/*.html").on("change", browserSync.reload({stream: true}));
//     gulp.watch("app/css/**/*.css").on("change", browserSync.reload({stream: true}));
//     gulp.watch("app/js/**/*.js").on("change", browserSync.reload({stream: true}));
//
//     browserSync.init({
//         server: "./dist"
//     });
// });
