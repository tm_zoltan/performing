$(document).ready(function() {

    var body = $("body");

    body.removeClass("no-js");
    body.addClass("js");

    //alert("this is a popup to test js");

    $('.responsiveslider').slick({
        dots: true,
        speed: 500,
        swipeToSlide: true,
        touchMove: true,
        mobileFirst: true,
        responsive: [
            {
              breakpoint: 480,
              settings: "unslick"
            }
        ]
    });

    var navToggle = $("#js__nav-toggle");
    var navMenu   = $("#js__nav");

    navToggle.on("click", function() {

        if (navMenu.hasClass("is-active")) {

            navMenu.removeClass("is-active");

        } else {

            navMenu.addClass("is-active");

        }

    });


});
